#!/bin/sh

./jira-report.py -f bccfile -o bcc-out.csv >jira-bcc-report.log
./jira-report.py -f acncfile -o acnc-out.csv >jira-acnc-report.log
./jira-report.py -f ecofile -o eco-out.csv >jira-eco-report.log
./jira-report.py -f barfile -o bar-out.csv >jira-bar-report.log
./jira-report.py -f acsfile -o acs-out.csv >jira-acs-report.log
./jira-report.py -f sacfile -o sac-out.csv >jira-sac-report.log

