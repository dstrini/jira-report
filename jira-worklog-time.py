#!/usr/bin/python
from jira.client import JIRA
import logging
import sys, getopt
import re
import datetime

logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)

log.info('starting up')


def timelog(timestring):
    hours = 0
    whours = 0
    dhours = 0
    hhours = 0
    mhours = 0
    weekmatch = re.search("(\d+)w",timestring)
    daymatch = re.search("(\d+)d",timestring)
    hourmatch = re.search("(\d+)h",timestring)
    minutematch = re.search("(\d+)m",timestring)

    if weekmatch:
        whours = int(weekmatch.group(1)) * 5 * 7
    if daymatch:
        dhours = int(daymatch.group(1)) * 7
    if hourmatch:
        hhours = int(hourmatch.group(1))
    if minutematch:
        mhours = float(minutematch.group(1))/60

    hours = whours + dhours + hhours + mhours
    return hours

def dateobj(string):
    datetoken = map(int,string.split("-"))
    dateobject = datetime.date(datetoken[0],datetoken[1],datetoken[2])
    return dateobject

def connect_jira(jira_server, jira_user, jira_password):
    '''
    Connect to JIRA. Return None on error
    '''
    try:
        log.info("Connecting to JIRA: %s" % jira_server)
        jira_options = {'server': jira_server}
        jira_svr = JIRA(options=jira_options,
                    # Note the tuple
                    basic_auth=(jira_user,
                                jira_password))
        return jira_svr
    except Exception,e:
        log.error("Failed to connect to JIRA: %s" % e)
        return None

def jira_pull(querystring,datefrom,dateto,outfile):

    jira = connect_jira('https://incognitosoftware.atlassian.net','relnotes','VbrsSgwYAGz9haceh4Cdoz')
    #jira.search.views.default.max = -1

    block_size = 1000
    block_num = 0
    total_num_issues = 0

    querystring = querystring + " and updatedDate > \"" + datefrom + " 00:01\" and updatedDate < \"" + dateto + " 23:59\""
    print querystring
    lowdate = dateobj(datefrom)
    highdate = dateobj(dateto)

    while True:
        start_idx = block_num*block_size

        issues = jira.search_issues(querystring,start_idx,block_size)   #"issue = DHCP-1 or issue = DHCP-2"

        num_issues = len(issues)
        total_num_issues += num_issues
        log.info("Block %s, %s issues" % (block_num, num_issues))
        block_num += 1
        total_time = 0

        if num_issues == 0:
            log.info("Finished retrieving information from %s issues" % total_num_issues)
            break

        if outfile is not None:
           fh = open(outfile,'a+')

        for issue in issues:
            wls = jira.worklogs(issue.key)
            datestr = dateobj(issue.fields.updated.split("T")[0])
#            print str(issue) + "::" + str(datestr)
            if wls:
                for wl in (wls):
                    fh.write(wl.started + "\n")
                    fh.write(wl.timeSpent + "\n")
                    fh.write(wl.comment + "\n")
                    fh.write(str(wl.author) + "\n")
                    wldate = dateobj(wl.started.split("T")[0])
#                    print "datestr: " + datestr + "wldate: " + wldate
#                    if wldate == datestr:
                    if wldate >= lowdate and wldate <= highdate:
                        #print "worklogdate: " + str(wldate) + " lowdate: " + str(lowdate) + " highdate: " + str(highdate)
                        #print wl.timeSpent
                        total_time += timelog(wl.timeSpent)
                        #print total_time
#                        sys.stdout.write('.')
#                        sys.stdout.flush()

                if outfile is not None:
                  fh.write(str(issue) + "\n")
                else:
                  print issue + "\n"
        print "\ntotal_time:" + str(total_time)

        if outfile is not None:
            fh.closed



def printhelp():
    print ''
    print 'Syntax:'
    print '    jira.py [-p <project> [-q <query>]] [-f <queryfile> -o <outputfile>] [-h]'
    print ''
    print '-f  <queryfile> : supply a queryfile that has the set of valid JIRA queries'
    print '                  one JIRA query per line'
    print '-o <outputfile> : supply a filename that will be created to output the results'
    print '                  of the JIRA query all appended into the one output file'
    print ''

def main(argv):
	querystring = None
        outfile = None

	try:
		opts, args = getopt.getopt(argv,"hp:q:f:o:1:2:",["project=","query=","infile=","outfile=","datefrom=","dateto="])
	except getopt.GetoptError:
		printhelp();
		sys.exit(2)
	for opt, arg in opts:
		if opt == '-h':
			printhelp()
			sys.exit()
		elif opt in ("-q", "--query"):
			querystring = arg
#			jira_pull(querystring,outfile)
                elif opt in ("-f", "--file"):
                     infile = arg
                elif opt in ("-1","--datefrom"):
                    date1 = arg
                elif opt in ("-2","--dateto"):
                    date2 = arg
                elif opt in ("-o","--outfile"):
                     outfile = arg
                     ofile = open(outfile,'w+')
                     ofile.truncate()
                     with open(infile) as inputfile:
                        for line in inputfile:
                           jira_pull(line,date1,date2,outfile)


if __name__ == "__main__":
   main(sys.argv[1:])
