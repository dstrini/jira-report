#!/usr/bin/python
from jira.client import JIRA
import logging
import sys, getopt
import re

logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)

log.info('starting up')

def connect_jira(jira_server, jira_user, jira_password):
    '''
    Connect to JIRA. Return None on error
    '''
    try:
        log.info("Connecting to JIRA: %s" % jira_server)
        jira_options = {'server': jira_server}
        jira_svr = JIRA(options=jira_options,
                    # Note the tuple
                    basic_auth=(jira_user,
                                jira_password))
        return jira_svr
    except Exception,e:
        log.error("Failed to connect to JIRA: %s" % e)
        return None

def jira_pull(projectstring,querystring,outfile):

	jira = connect_jira('https://incognitosoftware.atlassian.net','relnotes','VbrsSgwYAGz9haceh4Cdoz')
	#jira.search.views.default.max = -1

	block_size = 1000
	block_num = 0
	total_num_issues = 0
	while True:
		start_idx = block_num*block_size

		if projectstring is not None:

			issues = jira.search_issues('project in (' + projectstring + ') and ' + querystring,start_idx,block_size)   #"issue = DHCP-1 or issue = DHCP-2"
		else:
			issues = jira.search_issues(querystring,start_idx,block_size)   #"issue = DHCP-1 or issue = DHCP-2"

		num_issues = len(issues)
		total_num_issues += num_issues
		log.info("Block %s, %s issues" % (block_num, num_issues))
		block_num += 1

		if num_issues == 0:
			log.info("Finished retrieving information from %s issues" % total_num_issues)
			break

                if outfile is not None:
                   fh = open(outfile,'a+')

		for issue in issues:
                        datestr = issue.fields.created.split("T")[0]
                        link = 'https://incognitosoftware.atlassian.net/browse/' + issue.key
                        #wrap = '=HYPERLINK(CHAR(34)' + link + 'CHAR(34)CHAR(44)CHAR(34)' + issue.key + 'CHAR(34))'
                        sales = issue.fields.customfield_10302
                        engpri = issue.fields.customfield_10181
                        priorityname = issue.fields.priority.name
                        issuesummary = re.sub(r'"|\'|,','',issue.fields.summary)
                        if sales is not None:
                          salesstr = 'https://incognito.my.salesforce.com/' + sales
                        else:
                          salesstr = ' '
                        if engpri is None:
                          engpri  = ' '
                        if priorityname is None:
                            priorityname = ' '
			issuestr = issue.key + ',' + priorityname + ',' + issue.fields.status.name + ',' + '"' + issuesummary + '"' + ',' + datestr  + ',' + '"' + link + '"' + ',' + str(salesstr) + ',' + str(engpri)
                        if outfile is not None:
                          fh.write(issuestr + "\n")
                        else:
                          print issuestr + "\n"
                if outfile is not None:
                    fh.closed



def printhelp():
    print ''
    print 'Syntax:'
    print '    jira.py [-p <project> [-q <query>]] [-f <queryfile> -o <outputfile>] [-h]'
    print ''
    print '-f  <queryfile> : supply a queryfile that has the set of valid JIRA queries'
    print '                  one JIRA query per line'
    print '-o <outputfile> : supply a filename that will be created to output the results'
    print '                  of the JIRA query all appended into the one output file'
    print ''

def main(argv):
	projectstring = None
	querystring = None
        outfile = None

	try:
		opts, args = getopt.getopt(argv,"hp:q:f:o:",["project=","query=","infile=","outfile="])
	except getopt.GetoptError:
		printhelp();
		sys.exit(2)
	for opt, arg in opts:
		if opt == '-h':
			printhelp()
			sys.exit()
		elif opt in ("-p", "--project"):  #DHCP
			projectstring = arg
		elif opt in ("-q", "--query"):
			querystring = arg
			jira_pull(projectstring,querystring,outfile)
                elif opt in ("-f", "--file"):
                     infile = arg
                elif opt in ("-o","--outfile"):
                     outfile = arg
                     ofile = open(outfile,'w+')
                     ofile.truncate()
                     with open(infile) as inputfile:
                        for line in inputfile:
                           jira_pull(projectstring,line,outfile)


if __name__ == "__main__":
   main(sys.argv[1:])
